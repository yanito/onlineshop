# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-25 16:27+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: cart/forms.py:10
msgid "Quantity"
msgstr ""

#: coupons/forms.py:6
msgid "Coupon"
msgstr ""

#: onlineshop/settings.py:119
msgid "English"
msgstr ""

#: onlineshop/settings.py:120
msgid "Bulgarian"
msgstr ""

#: onlineshop/urls.py:24
msgid "admin/"
msgstr ""

#: onlineshop/urls.py:25
msgid "cart/"
msgstr ""

#: onlineshop/urls.py:26
msgid "orders/"
msgstr ""

#: onlineshop/urls.py:27
msgid "payment/"
msgstr ""

#: onlineshop/urls.py:28
msgid "coupons/"
msgstr ""

#: payment/urls.py:9
msgid "process/"
msgstr ""

#: payment/urls.py:10
msgid "done/"
msgstr ""

#: payment/urls.py:11
msgid "canceled/"
msgstr ""

#: shop/templates/shop/base.html:8 shop/templates/shop/base.html:15
msgid "My shop"
msgstr ""

#: shop/templates/shop/base.html:24
msgid "Your cart"
msgstr ""

#: shop/templates/shop/base.html:26
#, python-format
msgid ""
"              \n"
"                                %(items)s item, $%(total)s\n"
"                                "
msgid_plural ""
"              \n"
"                                %(items)s items, $%(total)s            \n"
"                            "
msgstr[0] ""
msgstr[1] ""

#: shop/templates/shop/base.html:37
msgid "Your cart is empty."
msgstr ""

#: shop/templates/shop/product/detail.html:17
msgid "Add to cart"
msgstr ""
